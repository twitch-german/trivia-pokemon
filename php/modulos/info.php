<?php 
$num = $_GET['p'];

if( ! isset( $pokemons[$num] ) ){
    //forzar MissingNo
    $pokemon = [
        'nombre' => 'MissingNo',
        'info' => file_get_contents( "php/missingNo.txt")
    ];
    $foto = 'MissingNo';
    $tipos = [ 'Buffer Overflow' ];
}else{
    $pokemon = $pokemons[$num];
    $foto = str_pad( $num + 1, 3, "0", STR_PAD_LEFT );
    $tipos = explode( ",", $pokemon['tipo'] );
}
?>
<article>
    <div>
        <h2><?php echo $pokemon['nombre']; ?></h2>
        <?php 
        array_map( function($t){
            echo "<span class='$t'>$t</span>";
        }, $tipos );
        ?>
        
    </div>
    <img class='<?php echo $tipos[0] ?? "fuego"; ?>' src="imagenes/large/<?php echo $foto; ?>.png" alt="<?php echo $pokemon['nombre']; ?>" />
    <p><?php echo $pokemon['info']; ?></p>


    <?php 
    $num = !preg_match( "/^\d+$/", $num ) ? -1 : $num;
    $prev = $num - 1;
    $next = $num + 1;
    $cant_max = count( $pokemons );
    if( $next >= $cant_max ) $next = 0;
    if( $prev < 0 || $prev >= $cant_max ) $prev = count( $pokemons ) - 1;
    ?>
    <ul id="joaquin141714">
        <li><a href='index.php?c=info&p=<?php echo $prev; ?>'>Anterior</a></li>
        <li><a href='index.php?c=info&p=<?php echo $next; ?>'>Siguiente</a></li>
    </ul>
</article>