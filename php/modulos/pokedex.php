        <h2>Pokedex</h2>
        <ul class='horizontal'>
            <?php 
            foreach( $pokemons as $indice => $pokemon ){
            $num = str_pad( $indice + 1, 3, "0", STR_PAD_LEFT );


            echo <<<HTML
            <li>
                <h3>$pokemon[nombre]</h3>
                <img src="imagenes/small/$num.png" alt="$pokemon[nombre]" />
                <a href='index.php?c=info&p=$indice'>VER INFO</a>
            </li>
HTML; 
            }
            ?>
        </ul>