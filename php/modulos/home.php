<h2>Bienvenidxs</h2>
        <p>Esta es la trivia de la famosa serie televisiva Pokemon, donde tienes que adivinar en base a una figura, de qué pokemon se trata, todos de la primera generación.<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br />Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        <h2>Últimos jugadores</h2>
        <ul class='horizontal'>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>100 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>100 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>100 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>100 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>100 puntos</p>
            </li>
        </ul>

        <h2>Top jugadores</h2>
        <ul class='horizontal'>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>1500 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>1300 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>1100 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>900 puntos</p>
            </li>
            <li>
                <h3>Nombre</h3>
                <img src="users/foto.png" alt="Nombre" />
                <p>100 puntos</p>
            </li>
        </ul>