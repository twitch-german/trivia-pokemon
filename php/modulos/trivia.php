<?php
if( isset($_GET['r'] ) ){
    $carpeta = 'large';
    $numero = $_GET['p'];
    $resultado = $_GET['r'];
    $pokemon = $pokemons[$numero-1];
    $nombre = $pokemon['nombre'];
}else{
    $carpeta = 'trivia';
    $nombre = null;
    $resultado = null;
    $numero = array_rand( $pokemons );
}

$numero = str_pad( $numero, 3, '0', STR_PAD_LEFT );

?>
<h2>¿Quién es ese Pokemon?</h2>
<div id="trivia">
    <span id="nombre"><?php echo $nombre; ?></span>
    <img width="350" src='/imagenes/<?php echo $carpeta; ?>/<?php echo $numero; ?>.png' alt="Pokemon" />
    <form method="post" action="adivinar.php">
        <?php 
        if( is_null( $resultado ) ):
        ?>

        <input type="text" list="diegootek_se_suscribio_al_canal" required name="pokemon" autocomplete="off"  />
        <datalist id="diegootek_se_suscribio_al_canal">
            <?php 
            $pokemons_ordenados = array_map( function($p){ return $p['nombre']; }, $pokemons );
            sort( $pokemons_ordenados );

            foreach( $pokemons_ordenados as $p ){
                echo "<option>$p</option>";
            } ?>
        </datalist>
        <input type="hidden" value="<?php echo $numero; ?>" name="numero" /> 
        <button>Jugar!</button>
        <?php else: ?>
            <p><?php echo $resultado == 1 ? 'ACERTASTE!' : 'PERDISTE! <a class="info" href="index.php?c=info&p='.($numero-1).'">info</a>'; ?></p>
            <a href='index.php?c=trivia'>Jugar de nuevo!</a>
        <?php endif; ?>
    </form>
</div>