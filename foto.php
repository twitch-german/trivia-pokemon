<?php 
$numero = $_GET['n'] ?? 'MissingNo.png';
$foto = imagecreatefrompng("imagenes/large/$numero.png");

imagefilter($foto, IMG_FILTER_BRIGHTNESS,-255);
imagesavealpha($foto, true);
header("Content-type: image/png");
imagepng($foto);