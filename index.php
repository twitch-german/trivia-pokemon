<?php 
include( 'php/pokemons.php' );
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>¿Quién es ese Pokèmon?</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/fuentes/pokemon.css">
    <link rel="stylesheet" href="assets/estilos.css">
</head>
<body>
    <header>
        <h1>Trivia Pokémon</h1>
        <nav>
            <ul>
                <li><a href='index.php?c=home'>Home</a></li>
                <li><a href='index.php?c=pokedex'>Pokedex</a></li>
                <li><a href='index.php?c=trivia'>Trivia</a></li>
            </ul>
        </nav>
    </header>
    <main>
    <?php 
    $categoria = $_GET['c'] ?? 'home';
    $categoria = preg_replace("/\W/", "__", $categoria );

    /*
    if( $categoria == 'home' ){
        include( 'php/modulos/home.php');
    }else if( $categoria == 'pokedex' ){
        include( 'php/modulos/pokedex.php');
    }else if( $categoria == 'trivia' ){
        include( 'php/modulos/trivia.php');
    }else if( $categoria == 'info' ){
        include( 'php/modulos/info.php');
    }else{
        include( 'php/modulos/404.php' );
    }
    */
    /*
    switch( $categoria ){
        case 'home': include( 'php/modulos/home.php'); break;
        case 'pokedex': include('php/modulos/pokedex.php' ); break;
        case 'info': include( 'php/modulos/info.php' ); break;
        case 'trivia': include( 'php/modulos/trivia.php' ); break;
        default: include( 'php/modulos/404.php' );
    }
    */
    $archivo_a_cargar = "php/modulos/$categoria.php";
    if( file_exists( $archivo_a_cargar ) ){
        include(  $archivo_a_cargar );
    }else{
        include( 'php/modulos/404.php' );
    }


    ?>
    </main>
</body>
</html>